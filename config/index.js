const app = require('./app');
const express = require('./express');
const routs = require('./routs');

module.exports ={
    app,
    express,
    routs,
};