const mongoose = require('mongoose');

const BookSchema = new mongoose.Schema({
    user_id:{
        type: String,
        required: true
    },
    car:{
        type: String,
        required: true
    },
    time_wait:{
        type: String,
        required: true
    },
    time_drive:{
        type: String,
        required: true
    },
    wait_price:{
        type: String,
        required: true
    },
    drive_price:{
        type: String,
        required: true
    }
});

mongoose.model('Book',BookSchema);